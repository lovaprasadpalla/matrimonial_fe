import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  imag:string='././'
  products:any=[];
  responsiveOptions :any =[]
  constructor() { this.responsiveOptions =[
    {
      breakpoint: '1024px',
      numVisible: 3,
      numScroll: 3
  },
  {
      breakpoint: '768px',
      numVisible: 2,
      numScroll: 2
  },
  {
      breakpoint: '560px',
      numVisible: 1,
      numScroll: 1
  }
    
  ] };

  ngOnInit(): void {

    this.products = [
      {
         "image": "d1.jpg",
          "name" :"RANJITH KUMAR THUMPALA",
          "id" :"profileId:VKMG4411",
          "DEMO": "created by:parent"
      },
      {
        "image": "d2.jpg",
        "name" :"SAMPATH KUMAR TARUNI",
        "id" :"profileId:VKMG4408",
        "DEMO": "created by:parent"
     },
     {
      "image": "d3.jpg",
      "name" :"SRI VIDYA PILLA",
      "id" :"profileId:VKMG4404",
      "DEMO": "created by:parent"
   },
   {
    "image": "d4.jpg",
    "name" :"SAI VAMSI GUDIVADA",
    "id" :"profileId:VKMG4403",
    "DEMO": "created by:parent"
 },
    ]
  }
  }


