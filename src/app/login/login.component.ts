import { Component, OnInit } from '@angular/core';


 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  public showPassword: boolean = false;
  
  constructor( ) {
    
   }
   show(){
    this.showPassword = !this.showPassword;
   }
  ngOnInit(): void {
    
  }

}
