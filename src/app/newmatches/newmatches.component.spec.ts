import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewmatchesComponent } from './newmatches.component';

describe('NewmatchesComponent', () => {
  let component: NewmatchesComponent;
  let fixture: ComponentFixture<NewmatchesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewmatchesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewmatchesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
